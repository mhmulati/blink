#!/usr/bin/env bash

INCDIR="${HOME}/include"
OP="-c"

if [ $# -ne 0 -a $# -ne 1 -a $# -ne 2 ]
then
	echo "$# is not the correct number of arguments"
	echo "Usage:"
	echo "$(basename ${0}) [[-c|-d] INCDIR]"
	echo "-c to create the links (default)"
	echo "-d to delete the links"
	echo "default INCDIR=${INCDIR}"
	exit 2
fi

if [ $# -ge 1 ]
then
	OP=${1}
	if [ "${OP}" != "-d" -a "${OP}" != "-c" ]
	then
		echo "Error: invalid ${OP}"
		exit 2
	fi
fi

if [ $# -ge 2 ]
then
	INCDIR=${2}
fi

if [ ! -d ${INCDIR} ]
then
	mkdir -p ${INCDIR}
fi

for DIR in `ls -vd */`
do
	if [ -d "${DIR}include" ]
	then
		for FILE in `ls -v ${DIR}include`
		do
			set -v
			if [ "${OP}" = "-c" ]
			then
				ln -srfv ${PWD}/${DIR}include/${FILE} ${INCDIR}
			elif [ ${OP} = "-d" ]
			then
				if [ -f ${PWD}/${DIR}include/${FILE} ]
				then
					rm -rfv ${INCDIR}/${FILE}
				fi
			fi
			set -
		done
	fi
done
