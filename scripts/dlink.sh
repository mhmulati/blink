#!/usr/bin/env bash 

ORIGPARENTDIR=.
OP=-c
DESTDIR=${HOME}

if [ $# -gt 3 ]; then
	echo "$# is not the correct number of arguments"
	echo "Usage: $(basename ${0}) [ORIGPARENTDIR [-c|-d [DESTDIR]]]"
	echo "-c to create the links (default)"
	echo "-d to delete the links"
	echo "Default: $(basename ${0}) ${ORIGPARENTDIR} -c ${DESTDIR}"
	exit 2
fi

if [ $# -ge 1 ]; then
	ORIGPARENTDIR=${1}
fi

if [ $# -ge 2 ]; then
	OP=${2}
	if [ "${OP}" != "-d" -a "${OP}" != "-c" ]; then
		echo "Error: invalid ${OP}"
		exit 2
	fi
fi

if [ $# -ge 3 ]; then
	DESTDIR=${3}
fi

if [ -d ${ORIGPARENTDIR} ]; then
	for ORIGDIR in $( ls ${ORIGPARENTDIR} -vd */ | sed 's,/$,,' ); do
		if [ ${ORIGDIR} != . ]; then
			blink ${ORIGDIR} ${OP} ${DESTDIR}
		fi
	done
fi
