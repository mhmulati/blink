#!/usr/bin/env bash

USEVERSION=-v
ORIGDIR=.
OP=-c
DESTDIR=${HOME}

if [ $# -gt 4 ]; then
	echo "$# is not the correct number of arguments"
	echo "Usage: $(basename ${0}) [ [-v|-n] [ORIGDIR [-c|-d [DESTDIR]]]]"
	echo "-c to create the links (default)"
	echo "-d to delete the links"
	echo "Default: $(basename ${0}) ${USEVERSION} ${ORIGDIR} ${OP} ${DESTDIR}"
	exit 2
fi

if [ $# -ge 1 ]; then
	USEVERSION=${1}
	if [ "${USEVERSION}" != "-v" -a "${USEVERSION}" != "-n" ]; then
		echo "Error: invalid ${USEVERSION}"
		exit 2
	fi
fi

if [ $# -ge 2 ]; then
	ORIGDIR=${2}
fi

if [ $# -ge 3 ]; then
	OP=${3}
	if [ "${OP}" != "-d" -a "${OP}" != "-c" ]; then
		echo "Error: invalid ${OP}"
		exit 2
	fi
fi

if [ $# -ge 4 ]; then
	DESTDIR=${4}
fi

ORIGDIRBIN=${ORIGDIR}/bin
DESTDIRBIN=${DESTDIR}/bin

VERSION=""
if [ "${USEVERSION}" = "-v" ]; then
	if [ -f ${ORIGDIR}/VERSIONo ]; then
		VERSION=$( cat ${ORIGDIR}/VERSIONo )
	elif [ -f ${ORIGDIR}/VERSION ]; then
		VERSION="-$( cat ${ORIGDIR}/VERSION )"
	fi
fi

# echo "ORIGDIR=${ORIGDIR}"
# echo "DESTDIR=${DESTDIR}"
# echo "ORIGDIRBIN=${ORIGDIRBIN}"
# echo "DESTDIRBIN=${DESTDIRBIN}"
# echo "VERSION=${VERSION}"

if [ -d ${ORIGDIRBIN} ]; then
	for FILE in $( ls -v ${ORIGDIRBIN} ); do
#		echo "FILE=${FILE}"
#		set -v
		if [ ! -d ${DESTDIRBIN} ]; then
			mkdir -p ${DESTDIRBIN}
		fi

		if [ "${OP}" = "-c" ]; then
			if [ "$( readlink -f ${DESTDIRBIN}/${FILE}${VERSION} )" != "$( readlink -f ${ORIGDIRBIN}/${FILE} )" ]; then
				ln -srfv ${ORIGDIRBIN}/${FILE} ${DESTDIRBIN}/${FILE}${VERSION}
			fi
		elif [ ${OP} = "-d" ]; then
			rm -fv ${DESTDIRBIN}/${FILE}${VERSION}
		fi
#		set -
	done
fi
