#!/usr/bin/env bash

LIBDIR="${HOME}/lib"
OP="-c"

if [ $# -ne 0 -a $# -ne 1 -a $# -ne 2 ]
then
	echo "$# is not the correct number of arguments"
	echo "Usage:"
	echo "$(basename ${0}) [[-c|-d] LIBDIR]"
	echo "-c to create the links (default)"
	echo "-d to delete the links"
	echo "default LIBDIR=~/lib"
	exit 2
fi

if [ $# -ge 1 ]
then
	OP=${1}
	if [ "${OP}" != "-d" -a "${OP}" != "-c" ]
	then
		echo "Error: invalid ${OP}"
		exit 2
	fi
fi

if [ $# -ge 2 ]
then
	LIBDIR=${2}
fi

if [ ! -d ${LIBDIR} ]
then
	mkdir -p ${LIBDIR}
fi

for DIR in `ls -vd */`
do
	if [ -d "${DIR}lib" ]
	then
		for FILE in `ls -v ${DIR}lib`
		do
			set -v
			if [ "${OP}" = "-c" ]
			then
				ln -srfv ${PWD}/${DIR}lib/${FILE} ${LIBDIR}
			elif [ ${OP} = "-d" ]
			then
				if [ -f ${PWD}/${DIR}lib/${FILE} ]
				then
					rm -fv ${LIBDIR}/${FILE}
				fi
			fi
			set -
		done
	fi
done
